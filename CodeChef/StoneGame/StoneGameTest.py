'''
Created on Feb 11, 2012

@author: Owner
'''
import unittest
from StoneGame import StoneGame

class Test(unittest.TestCase):


    def testOnePile(self):
        expected = "ALICE"
        real = StoneGame().solveComplex([1])
        self.assertEqual(expected, real)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testOnePile']
    unittest.main()