'''
Created on Feb 10, 2012

@author: Owner
'''
import unittest
from Odd import Odd

class Test(unittest.TestCase):


    def testOdd1(self):
        expected = 1
        real = Odd().solveComplex(1)
        self.assertEqual(expected, real)

    def testOdd12(self):
        expected = 2
        real = Odd().solveComplex(2)
        self.assertEqual(expected, real)

    def testOdd123(self):
        expected = 2
        real = Odd().solveComplex(3)
        self.assertEqual(expected, real)
    
    def testOdd1234(self):
        expected = 4
        real = Odd().solveComplex(4)
        self.assertEqual(expected, real)

    def testOdd12345(self):
        expected = 4
        real = Odd().solveComplex(5)
        self.assertEqual(expected, real)

    def testOdd123456(self):
        expected = 4
        real = Odd().solveComplex(6)
        self.assertEqual(expected, real)
        
    def testOdd1234567(self):
        expected = 4
        real = Odd().solveComplex(7)
        self.assertEqual(expected, real)
    
    def testOdd12345678(self):
        expected = 8
        real = Odd().solveComplex(8)
        self.assertEqual(expected, real)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testOdd1']
    unittest.main()