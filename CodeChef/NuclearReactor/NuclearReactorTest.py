'''
Created on Feb 12, 2012

@author: Owner
'''
import unittest
from NuclearReactor import NuclearReactor

class Test(unittest.TestCase):


    def testOneReactorWithoutBombarded(self):
        expected = "0"
        numberOfBombards = 0
        maxParticlesPerReactor = 1
        numberOfReactors = 1
        real = NuclearReactor().solveComplex(numberOfBombards,maxParticlesPerReactor,numberOfReactors)
        self.assertEqual(expected, real)

    def testOneReactorWithOneBombard(self):
        expected = "1"
        numberOfBombards = 1
        maxParticlesPerReactor = 1
        numberOfReactors = 1
        real = NuclearReactor().solveSimple(numberOfBombards,maxParticlesPerReactor,numberOfReactors)
        self.assertEqual(expected, real)

    def testTwoReactorsWithoutBombarded(self):
        expected = "0 0"
        numberOfBombards = 0
        maxParticlesPerReactor = 1
        numberOfReactors = 2
        real = NuclearReactor().solveSimple(numberOfBombards,maxParticlesPerReactor,numberOfReactors)
        self.assertEqual(expected, real)
        
    def testTwoReactorsWithOneBombard(self):
        expected = "1 0"
        numberOfBombards = 1
        maxParticlesPerReactor = 1
        numberOfReactors = 2
        real = NuclearReactor().solveSimple(numberOfBombards,maxParticlesPerReactor,numberOfReactors)
        self.assertEqual(expected, real)
        
    def testTwoReactorsWithThreeBombard(self):
        expected = "2 1"
        numberOfBombards = 5
        maxParticlesPerReactor = 2
        numberOfReactors = 2
        real = NuclearReactor().solveSimple(numberOfBombards,maxParticlesPerReactor,numberOfReactors)
        self.assertEqual(expected, real)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testOneReactor']
    unittest.main()