'''
Created on Feb 14, 2012

@author: Owner
'''
import unittest
from PermutationCycle import PermutationCycle

class Test(unittest.TestCase):


    def test1(self):
        expected = [[1,1]]
        real = PermutationCycle().solve([1])
        self.assertEqual(expected, real)

    def test12(self):
        expected = [[1,1],[2,2]]
        real = PermutationCycle().solve([1,2])
        self.assertEqual(expected, real)
        
    def test21(self):
        expected = [[1,2,1]]
        real = PermutationCycle().solve([2,1])
        self.assertEqual(expected, real)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testOneLengthPermutation']
    unittest.main()