'''
Created on Feb 9, 2012

@author: Owner
'''
import unittest
from ATM import ATM

class Test(unittest.TestCase):


    def testWithdraw40With120WithCharge(self):
        expected = 79.50
        result = ATM().withdraw(40,120)
        self.assertEqual(expected, result)

    def testWithdraw150With120WithCharge(self):
        expected = 120
        result = ATM().withdraw(150,120)
        self.assertEqual(expected, result)

    def testWithdrawNotMultipleOf5(self):
        expected = 120
        result = ATM().withdraw(11,120)
        self.assertEqual(expected, result)

    def testWithdraw120With120WithCharge(self):
        expected = 120
        result = ATM().withdraw(120,120)
        self.assertEqual(expected, result)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCorrectWithdraw']
    unittest.main()