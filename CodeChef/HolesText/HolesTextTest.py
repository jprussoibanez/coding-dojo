'''
Created on Feb 12, 2012

@author: Owner
'''
import unittest
from HolesText import HolesText

class Test(unittest.TestCase):


    def testA(self):
        expected = 1
        real = HolesText().solve("A")
        self.assertEquals(expected, real)

    def testB(self):
        expected = 2
        real = HolesText().solve("B")
        self.assertEquals(expected, real)
    
    def testAB(self):
        expected = 3
        real = HolesText().solve("AB")
        self.assertEquals(expected, real)        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()