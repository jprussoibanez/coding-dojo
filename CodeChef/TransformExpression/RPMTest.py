'''
Created on Feb 11, 2012

@author: Owner
'''
import unittest
from RPM import RPM

class RPMTest(unittest.TestCase):


    def testOneNumber(self):
        expected = "1"
        real = RPM().solveComplex("1")
        self.assertEqual(expected, real)

    def testOneSum(self):
        expected = "12+"
        real = RPM().solveComplex("1+2")
        self.assertEqual(expected, real)

    def testOneSumWithParenthesis(self):
        expected = "12+"
        real = RPM().solveComplex("(1+2)")
        self.assertEqual(expected, real)

    def testTwoSumWithParenthesis(self):
        expected = "12+34++"
        real = RPM().solveComplex("((1+2)+(3+4))")
        self.assertEqual(expected, real)

    def testTwoSeparateSumWithParenthesis(self):
        expected = "12+3+"
        real = RPM().solveComplex("(1+2)+3)")
        self.assertEqual(expected, real)
    
    def testLongExpression(self):
        expected = "at+bac++cd+^*"
        real = RPM().solveComplex("((a+t)*((b+(a+c))^(c+d)))")
        self.assertEqual(expected, real)
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()