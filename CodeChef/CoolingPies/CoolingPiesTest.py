'''
Created on Feb 12, 2012

@author: Owner
'''
import unittest
from CoolingPies import CoolingPies

class Test(unittest.TestCase):


    def testOnePieRack(self):
        expected = 1
        piesWeight = [10]
        rackWeight = [10]
        real = CoolingPies().solve(piesWeight,rackWeight)
        self.assertEqual(expected, real)

    def testTwoPieRack(self):
        expected = 2
        piesWeight = [10,20]
        rackWeight = [10,20]
        real = CoolingPies().solve(piesWeight,rackWeight)
        self.assertEqual(expected, real)

    def testTwoPieRackWithDifferentOrder(self):
        expected = 2
        piesWeight = [10,20]
        rackWeight = [20,10]
        real = CoolingPies().solve(piesWeight,rackWeight)
        self.assertEqual(expected, real)

    def testFullRackWithFullPies(self):
        expected = 4
        piesWeight = [10,20,30,40,50]
        rackWeight = [60,30,20,10,5]
        real = CoolingPies().solve(piesWeight,rackWeight)
        self.assertEqual(expected, real)
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testOnePieRack']
    unittest.main()