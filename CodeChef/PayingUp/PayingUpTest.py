'''
Created on Feb 11, 2012

@author: Owner
'''
import unittest
from PayingUp import PayingUp

class Test(unittest.TestCase):

    def testOneCoin(self):
        expected = "No"
        real = PayingUp().solveComplex(3,[5])
        self.assertEquals(expected, real)

    def testOneCoinWithDemand(self):
        expected = "Yes"
        real = PayingUp().solveComplex(3,[3])
        self.assertEquals(expected, real)        

    def testLotsOfCoinWithDemand(self):
        expected = "Yes"
        real = PayingUp().solveComplex(5,[3,4,7,8,9,12,2,2])
        self.assertEquals(expected, real)        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test']
    unittest.main()