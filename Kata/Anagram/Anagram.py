'''
Created on Feb 5, 2012

@author: Owner
'''

class Anagram(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass
    

    def isAnagram(self, word, wordDictionary):        
        return set(list(wordDictionary)) == set(list(word))

    def solveComplex(self,word,dictionary):
        result = set({})
        for wordDictionary in dictionary:
            if self.isAnagram(word, wordDictionary):
                result.add(wordDictionary)
        return result