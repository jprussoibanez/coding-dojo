'''
Created on Feb 5, 2012

@author: Owner
'''
import unittest
from Anagram import Anagram

class Test(unittest.TestCase):

    def testEmptyDictionary(self):
        anagram = Anagram()
        expected = set({})
        real = anagram.solveComplex("a",set({}))
        self.assertEqual(expected, real)

    def testWordDictionary(self):
        anagram = Anagram()
        expected = set({"a"})
        real = anagram.solveComplex("a",set({"a"}))
        self.assertEqual(expected, real)

    def testTwoWordDictionary(self):
        anagram = Anagram()
        expected = set({"a"})
        real = anagram.solveComplex("a",set({"a","b"}))
        self.assertEqual(expected, real)

    def testWordTwoDictionary(self):
        anagram = Anagram()
        expected = set({"ab","ba"})
        real = anagram.solveComplex("ab",set({"ab","ba"}))
        self.assertEqual(expected, real)

    def testLargeWordDictionary(self):
        anagram = Anagram()
        expected = set({"abcde","dcbae","edcba"})
        real = anagram.solveComplex("bcdea",set({"abcde","dcbae","edcba","ab","ba","abc"}))
        self.assertEqual(expected, real)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()