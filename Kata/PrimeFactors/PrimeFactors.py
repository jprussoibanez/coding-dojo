'''
Created on Feb 5, 2012

@author: Owner
'''

class PrimeFactors(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass
    
    def generate(self,number):
        result = []
        prime = 2        
        while number > 1:
            while number % prime == 0:
                result.append(prime)
                number /= prime
            prime += 1
                
        return result