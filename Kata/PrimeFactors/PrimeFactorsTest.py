'''
Created on Feb 5, 2012

@author: Owner
'''
import unittest
from PrimeFactors import PrimeFactors

class Test(unittest.TestCase):


    def testOne(self):
        expected = []
        real = PrimeFactors().generate(1)
        self.assertEqual(expected,real)
        
    def testTwo(self):
        expected = [2]
        real = PrimeFactors().generate(2)
        self.assertEqual(expected,real)

    def testThree(self):
        expected = [3]
        real = PrimeFactors().generate(3)
        self.assertEqual(expected,real)

    def testFour(self):
        expected = [2,2]
        real = PrimeFactors().generate(4)
        self.assertEqual(expected,real)
        
    def testSix(self):
        expected = [2,3]
        real = PrimeFactors().generate(6)
        self.assertEqual(expected, real)

    def testEight(self):
        expected = [2,2,2]
        real = PrimeFactors().generate(8)
        self.assertEqual(expected, real)

    def testNine(self):
        expected = [3,3]
        real = PrimeFactors().generate(9)
        self.assertEqual(expected, real)

    def testLarge(self):
        expected = [2,3,7,13]
        real = PrimeFactors().generate(2*3*7*13)
        self.assertEqual(expected, real)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()