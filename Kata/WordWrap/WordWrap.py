'''
Created on Feb 5, 2012

@author: Owner
'''
import string

class WordWrap(object):
    '''
    classdocs
    '''

    def __init__(self,lineWidth):
        '''
        Constructor
        '''
        self.lineWidth = lineWidth
    
    def wrap(self,line):
        space = string.rfind(line[0:self.lineWidth]," ")
        if len(line) <= self.lineWidth:
            resultText = line
        else:
            if space > 0:
                resultText = self.breakBetween(line,space,space+1)
            else:         
                resultText = self.breakBetween(line,self.lineWidth,self.lineWidth)
            
        return resultText
        
    def breakBetween(self,line,start,end):
        return line[0:start].strip() + "\n" + self.wrap(line[end:len(line)].strip())