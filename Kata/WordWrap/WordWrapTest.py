'''
Created on Feb 5, 2012

@author: Owner
'''
import unittest
from WordWrap import WordWrap

class Test(unittest.TestCase):


    def testEmptyLine(self):
        expected = ""
        real = WordWrap(10).wrap("")
        self.assertEqual(expected, real)

    def testOneWord(self):
        expected = "word"
        real = WordWrap(10).wrap("word")
        self.assertEqual(expected, real)

    def testLongWordShouldWrapAtLenght(self):
        expected = "word\nLong"
        real = WordWrap(4).wrap("wordLong")
        self.assertEqual(expected, real)

    def testLongerWordShouldWrapTwiceAtLenght(self):
        expected = "word\nLong\nLong"
        real = WordWrap(4).wrap("wordLongLong")
        self.assertEqual(expected, real)

    def testTwoWordsLongerThanLimitShouldWrap(self):
        expected = "word\nword"
        real = WordWrap(6).wrap("word word")
        self.assertEqual(expected, real)

    def testThreeWordsLongerThanLimitShouldWrap(self):
        expected = "word word\nword"
        real = WordWrap(12).wrap("word word word")
        self.assertEqual(expected, real)

    def testTwoWordsLongerWithLongerWhiteSpaceThanLimitShouldWrap(self):
        expected = "word\nword"
        real = WordWrap(6).wrap("word  word")
        self.assertEqual(expected, real)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testEmptyLine']
    unittest.main()